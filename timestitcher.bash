#!/bin/bash

. lxlanddaq01_check.bash

. /u/land/fake_cvmfs/sourceme.sh

while :
do
	$UCESB_DIR/empty/empty stream://lxlanddaq01:9100 \
		--time-stitch=wr,4000 \
		--server=size=1Gi,dropold=5,trans:8000,bufsize=2Mi \
		--server=size=1Gi,dropold=5,stream:9000,bufsize=2Mi \
		--debug --allow-errors
	sleep 5
done
