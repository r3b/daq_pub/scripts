# Drasi DAQ common.

branch=$(basename $0 | sed -n 's/\(.*\)_[^_]*/\1/p')
[ "$branch" = "drasi" ] && branch=main

do_gui=
is_stone_faced_killer=
while [ 0 -lt $# ]
do
	if [ "x-h" = "x$1" ]
	then
		echo "Usage: $0 [-h] [-gui] [-stone-faced-killer] <branch>"
		echo " -h                   Halp!"
		echo " -gui                 Will ask for confirmation via GUI dialogs."
		echo " -stone-faced-killer  Don't even ask questions later."
		echo " branch               Name of specific DAQ branch when there are several."
		echo "The branch can also be chosen with symlinks, e.g.:"
		echo " ln -s drasi_setup.bash los_setup.bash -> branch=los"
		exit 0
	elif [ "x-gui" = "x$1" ]
	then 
		do_gui=1
	elif [ "x-stone-faced-killer" = "x$1" ]
	then
		is_stone_faced_killer=1
	else
		branch=$1
	fi
	shift
done

# Dump node setup into variables, "main.conf" is the default.
echo Branch=\"$branch\"
branch_conf=$scripts/../$branch.conf
if [ ! -f $branch_conf ]
then
	echo "$branch_conf: is not a file." 1>&2
	exit 1
fi
. $branch_conf

this_host=$(hostname | cut -d'.' -f1 | tr '[:upper:]' '[:lower:]')
this_line=$(echo $MASTER $SLAVE | tr ' ' '\n' | grep $this_host)
this_type=$(echo $this_line | cut -d':' -f2)
this_subtype=$(echo $this_line | cut -d':' -f3)
this_ctrl=$(echo $this_line | cut -d':' -f4)
this_max_ev_size=$(echo $this_line | cut -d':' -f5)
this_buf_size=$(echo $this_line | cut -d':' -f6)
this_fca=$(echo $this_line | cut -d':' -f7)
this_cvt=$(echo $this_line | cut -d':' -f8)
this_name=$(echo $this_line | cut -d':' -f9)

if [ "$this_line" -a $this_fca -ge $this_cvt ]
then
	echo "FCA=$this_fca (arg7) >= CVT=$this_cvt (arg8)!" 1>&2
	echo "Read the GSI TRIVA module manual for more info!" 1>&2
	exit 1
fi

# Clean hosts.
EB_HOST=$(echo $EB | cut -d':' -f1)
EB_DRASI_PORT=$(echo $EB | cut -d':' -f2)
EB_TRANS_PORT=$(echo $EB | cut -d':' -f3)
EB_STREAM_PORT=$(echo $EB | cut -d':' -f4)
EB_SERVE_PORT=$(echo $EB | cut -d':' -f5)
EB_MAX_EV_SIZE=$(echo $EB | cut -d':' -f6)
EB_BUF_SIZE=$(echo $EB | cut -d':' -f7)
EB_NAME=$(echo $EB | cut -d':' -f8)
[ "$EB_DRASI_PORT" ] && EB_DRASI_PORT_OPT="--port=$EB_DRASI_PORT"
[ "$EB_DRASI_PORT" ] && EB_DRASI_PORT_SUFF=":$EB_DRASI_PORT"
[ "$EB_TRANS_PORT" ] && EB_TRANS_PORT_SUFF=":$EB_TRANS_PORT"
[ "$EB_STREAM_PORT" ] && EB_STREAM_PORT_SUFF=":$EB_STREAM_PORT"
[ "$EB_SERVE_PORT" ] && EB_SERVE_PORT_SUFF=":$EB_SERVE_PORT"
[ "$EB_NAME" ] || EB_NAME="Alien"
MASTER=${MASTER%%:*}
MASTER_USER=${MASTER%%@*}
MASTER=${MASTER#*@}
[ "x$MASTER_USER" == "x$MASTER" ] && MASTER_USER=$(whoami)
HOST_ARRAY=$MASTER
SLAVE_ARRAY=
DRASI_ARRAY=()
DRASI_SLAVE_ARRAY=()
for slave in $SLAVE
do
	host=${slave%%:*}
	user=${host%%@*}
	host=${host#*@}
	[ "x$user" == "x$host" ] && user=$(whoami)
	SLAVE_ARRAY="$SLAVE_ARRAY $user@$host"
	HOST_ARRAY="$HOST_ARRAY $host"
	DRASI_ARRAY+=("--drasi=$host")
	DRASI_SLAVE_ARRAY+=("--slave=$host")
done

# Some branches are special
if [[ "x$branch" == "xs2" ]] ; then
	:
fi

# Give a somewhat unique screen name.
exp_name=$(cd $scripts && pwd | sed 's,.*/\([^/]*\)/scripts,\1,')
session=${branch}_${exp_name}_drasi

# Makes a short-form version of given hostname.
function shortify()
{
	shortname=$(echo $1 | sed 's/r4l-/4l/;s/r4-/4/;s/x86l-/x/')
}

function ask_to_kill()
{
	[ "$is_stone_faced_killer" ] && return
	msg=$1
	do_it=
	if [ "$do_gui" ]
	then
		xmessage -geometry 400x300 -buttons Yes:0,No:1 $msg
		[ 0 -eq $? ] && do_it=1
	else
		echo
		echo $msg
		echo
		read -p "[y/Y=yes, anything else=no]: " -n 1 yes
		echo
		[[ $yes =~ ^[Yy]$ ]] && do_it=1
	fi
	if [ ! "$do_it" ]
	then
		echo I will not kill. I promise.
		exit 0
	fi
}
