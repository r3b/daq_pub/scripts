echo $1 | grep r4l- && host_type=r4l
echo $1 | grep x86l- && host_type=x86l
if [ ! "$host_type" ]
then
	echo "$1: Unknown host type!" 1>&2
	exit 1
fi
