#!/bin/bash

set -e

cd $EXP_PATH

source to.conf #|| { echo not found to.conf ; exit 1 }

mkdir -v ${TO_PC}:${TO_PORT}

cp -iv scripts/to_template.bash ${TO_PC}:${TO_PORT}/to.bash
chmod a+x ${TO_PC}:${TO_PORT}/to.bash
cp -iv scripts/serv_to.bash ${TO_PC}:${TO_PORT}
chmod a+x ${TO_PC}:${TO_PORT}/serv_to.bash
