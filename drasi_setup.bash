#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..
. scripts/drasi_common.bash

. explogin $(basename $PWD )

echo
echo "This will setup drasi DAQ \"$branch\", but not start it."
echo

function create()
{
	host=$1
	shortify $host
	screen -S $session -X screen -t $shortname
}

screen -ls | grep -q $session
if [ 0 -eq $? ]
then
	ask_to_kill "Screen session $session already running, kill?"
	echo "Killing $session..."
	screen -S $session -X quit
	sleep 2
fi

echo "Starting $session..."
screen -dmS $session
screen -S $session -p 0 -X title rate
screen -S $session -X stuff "cd $EXP_PATH
./drasi/bin/lwrocmon $EB_HOST$EB_DRASI_PORT_SUFF --rate
"
screen -S $session -X screen -t tree
screen -S $session -X stuff "cd $EXP_PATH
./drasi/bin/lwrocmon $EB_HOST$EB_DRASI_PORT_SUFF --tree
"
screen -S $session -X screen -t log
screen -S $session -X screen -t serv

create $EB_HOST$EB_DRASI_PORT_SUFF
for host in $HOST_ARRAY
do
	create $host
done

screen -S $session -X screen -t misc

echo Done.
echo
echo "Don't forget to run $scripts/${branch}_start.bash!"
echo
