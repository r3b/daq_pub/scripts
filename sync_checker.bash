#!/bin/bash

while true
do
	file=$(ls /d/land5/${EXP_NAME}/lmd/main0*.lmd | tail -n2 | head -n1)
	/u/htoernqv/toys/ucesb/empty/empty $file --input-buffer=135Mi --tstamp-print --colour=yes --max-events=1000000 | tail -n 20
	sleep 60
done
