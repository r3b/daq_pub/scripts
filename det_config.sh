#!/bin/bash

echo What detector would you like?
read detector
echo What is the name of your PC? Include x86l or r4l. For example, r4l-11
read PC
echo What kind of electronics am I using? Options are FEBEX, TAMEX, KILOM, VME
read ELECTRONIC
echo What eventbuilder number would you like?
read EB
echo Pick a number between 01-99. Remember this number for the unpacker.
read CTRL
echo Are you adding any other computers? YES or NO
read MAIN

if [ "$MAIN" == "YES" ]
then 
	echo What would you like to label the 1st detector as? 
	read det
else
	det=$detector
fi

KILOM=KILOM
TAMEX=TAMEX
VME=VME
FEBEX=FEBEX

if [ "$ELECTRONIC" == "$FEBEX" ]
then 
	echo $ELECTRONIC
	ID=101
fi
if [ "$ELECTRONIC" == "$TAMEX" ]
then 
	echo $ELECTRONIC
	ID=102
fi
if [ "$ELECTRONIC" == "$KILOM" ]
then 
	echo $ELECTRONIC
	ID=103
fi
if [ "$ELECTRONIC" == "$VME" ]
then 
	echo $ELECTRONIC
	ID=88
fi

filename=$detector.conf

echo $detector $PC $ELECTRONIC $ID $EB $CTRL $filename

printf "# Lord of the DAQ 
TO=lxlanddaq01 
# EB=host:drasi-port:trans-port:stream-port:serve-port:max_ev_size:buf_size:name (no port = defaults) 
EB=lxir133:$EB:$(($EB+1)):$(($EB+2)):$(($EB+3)):0x550000:300Mi:$detector 
# MASTER/SLAVE=host:type:subtype:ctrl:max_ev_size:buf_size:fca:cvt:name ... 
MASTER=$PC:$ID:$((ID*100)):$CTRL:10000:100Mi:10:20:$det \\" >> $filename

if [ "$MAIN" == "YES" ]
then 
	printf "\nSLAVE=\"" >> $filename
	echo How many other computers are you adding?
	read NUMBER
	for idet in $(seq $NUMBER)
	do
		echo $idet
		echo What detector would you like?
		read det1
		echo What is the name of the PC? Include x86l or r4l.
		read PC1
		echo What kind of electronics am I using? Options are FEBEX, TAMEX, KILOM, VME
		read ELECTRONIC1
		echo Pick a different number between 01-99. Remember this number for the unpacker.
		read CTRL1

		if [ "$ELECTRONIC1" == "$FEBEX" ]
		then 
			echo $ELECTRONIC1
			ID1=101
		fi
		if [ "$ELECTRONIC1" == "$TAMEX" ]
		then 
			echo $ELECTRONIC1
			ID1=102
		fi
		if [ "$ELECTRONIC1" == "$KILOM" ]
		then 
			echo $ELECTRONIC1
			ID1=103
		fi
		if [ "$ELECTRONIC1" == "$VME" ]
		then 
			echo $ELECTRONIC1
			ID1=88
		fi
		if [ $idet == $NUMBER ]
		then
			printf "\n$PC1:$ID1:$((ID1*100)):$CTRL1:10000:100Mi:10:20:$det1\n\"" >> $filename
			echo "\n$PC1:$ID1:$((ID1*100)):$CTRL1:10000:100Mi:10:20:$det1"
		else
			printf "\n$PC1:$ID1:$((ID1*100)):$CTRL1:10000:100Mi:10:20:$det1 \\" >> $filename
		fi
	done
fi
