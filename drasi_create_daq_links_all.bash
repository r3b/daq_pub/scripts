#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..
for branch in $(ls *.conf | sed 's/\.conf//')
do
	./scripts/drasi_create_daq_links.bash $branch
done
