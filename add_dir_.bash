if [ ! "$1" ]
then
	echo "Usage: $0 r4l-56" 1>&2
	echo "Will create r4l-56/ with standard files." 1>&2
	exit 1
fi
path=$scripts/../$1
if [ -d $path ]
then
	echo "$path already exists! I won't do nothin'." 1>&2
	exit 1
fi
mkdir $path
