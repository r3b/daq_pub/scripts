#!/bin/bash

cd $EXP_PATH/scripts

. colors.bash

. /u/land/opt/epics/epicsfind.bash
export EPICS_CA_ADDR_LIST=landgw01
export EPICS_CA_AUTO_ADDR_LIST=NO

hosts=$@

# Path to lwroc monitor
monitor=$EXP_PATH/drasi/bin/lwrocmon
opts="--rate"

# Timeout setup
timeout_down_notice=5
timeout_kick_default=15
# TODO: Use variable kick timeout
timeout_kick=${timeout_kick_default}

# Log file
logfile=../log/watcher.log

# How many times to try kicking a system automatically
# Note: It does not actually kick, yet
max_kick=3

function start_watcher
{
	$monitor $opts $1 2>&1
}

function mydate
{
	printf "%(%FT%T)T"
}

function now
{
	printf "%()T"
}

function timestamp
{
	printf "%(%s)T"
}

function pp
{
	clr=$1
	shift
	echo -e $clr $(now): $@ $CRESET
	echo -e $(mydate): $@ >> $logfile
}

function pp_ok
{
	pp $GREEN $@
}

function pp_warn
{
	pp $YELLOW $@
}

function pp_error
{
	pp $RED $@
}

function pp_action
{
	pp $CYAN $@
}

function pp_info
{
	pp $BLUE $@
}

ts=$(timestamp)
i=0
for h in $hosts
do
	echo "Starting watcher for host $h"
	group[$i]=$(grep MASTER ../*.conf | \
		grep $h | \
		sed "s#../\(.*\).conf.*#\1#")
	echo "group[$i] = ${group[$i]}, ${#group[$i]}"
	if [[ ${#group[$i]} -gt 0 ]] ; then
		echo "  Found matching group config: '${group[$i]}'"
	else
		echo "  Could not find matching group config. Cannot kick it."
	fi
	coproc proc { start_watcher $h; }
	w[$i]=$proc
	host[$i]=$h
	stat[$i]=-1
	last_stat[$i]=-1
	last_kicked[$i]=$ts
	n_kicked[$i]=0
	last_up[$i]=$ts
	last_down[$i]=$ts
	echo "w = $w"
	echo "w[$i] = ${w[$i]}"
	echo "w[$i][0] = ${w[$i][0]}"
	echo "w[$i][1] = ${w[$i][1]}"
	((i++))
done

while :
do
	# echo "------- cycle"
	ts=$(timestamp)

	i=0
	for h in $hosts
	do
		hostname=${host[$i]}
		while read -t 0.1 -u "${w[$i][0]}"
		do
			line=$REPLY
			ret=$?

			# echo "${host[$i]}, $h: $ret, $line"

			# Skip headers and startup messages
			if [[ "$line" =~ "known" ]] ; then continue; fi
			if [[ "$line" =~ "Status" ]] ; then continue; fi
			if [[ "$line" =~ "B/s" ]] ; then continue; fi
			if [[ "${#line}" -lt 1 ]] ; then continue; fi

			if [[ "$line" =~ "run" ]] ; then
				stat[$i]=0
				last_up[$i]=$ts
				n_kicked[$i]=0
			elif [[ "$line" =~ "inspill" ]] ; then
				stat[$i]=0
				last_up[$i]=$ts
				n_kicked[$i]=0
			elif [[ "$line" =~ "offspill" ]] ; then
				stat[$i]=0
				last_up[$i]=$ts
				n_kicked[$i]=0
			else
				# Reset last_kicked on transition to down
				if [[ ${stat[$i]} -eq 0 ]] ; then
					last_kicked[$i]=$ts
				fi

				stat[$i]=1
				last_down[$i]=$ts

				# Blabber about the status
				if [[ "$line" =~ "ReInit" ]] ; then
					pp_warn "DAQ on $hostname re-initialising."
				elif [[ "$line" =~ "Readout" ]] ; then
					pp_warn "DAQ on $hostname starting readout."
				else
					pp_warn "DAQ on $hostname is FAILING ($line)."
				fi
			fi

		done

		# Update EPICS records in daqioc
		if [ ${stat[$i]} -ne 0 ] && [ ${last_stat[$i]} -eq 0 ] ; then
			caput "r3b:daq:${group[$i]}:status" 0 > /dev/null
		fi
		if [ ${stat[$i]} -eq 0 ] && [ ${last_stat[$i]} -ne 0 ] ; then
			caput "r3b:daq:${group[$i]}:status" 1 > /dev/null
		fi

		last_stat[$i]=${stat[$i]}
		((i++))

	done

	# echo "stat = ${stat[*]}"

	# Action (drasi)

	# Status output at most every 2 seconds
	if [[ $ts -le $last ]] ; then
		continue
	fi
	last=$((ts + 1))

	i=0
	for h in $hosts
	do
		hostname=${host[$i]}

		if [[ ${stat[$i]} -ne 0 ]] ; then
			diff_down=$(($ts - ${last_up[$i]}))
			diff_kicked=$(($ts - ${last_kicked[$i]}))
			secs_to_kick=$(($timeout_kick - $diff_kicked))

			if  [[ $diff_down -gt $timeout_down_notice ]] ; then

				if [[ ${n_kicked[$i]} -lt $max_kick ]] ; then
					pp_action "$hostname is down for $diff_down s. Will kick in $secs_to_kick s (tried for ${n_kicked[$i]} of $max_kick times)."

					if [[ $diff_kicked -gt $timeout_kick ]] ; then
						pp_action "--- KICKING $hostname! ---"
						./${group[$i]}_killemall.bash -stone-faced-killer
						sleep 1

						./${group[$i]}_start.bash

						last_kicked[$i]=$ts
						((n_kicked[$i]++))
						# todo
					fi
				else
					pp_action "PERMANENT FAILURE ON $hostname. USER INTERACTION REQUIRED!"
					pp_action "Restart this watcher to get rid of this message."
					stat[$i]=2
				fi
			fi
		fi
		((i++))
	done

	# Check all system status
	i=0
	ok=""
	notok=""
	limbo=""

	for h in $hosts
	do
		if [[ ${#group[$i]} -gt 0 ]] ; then
			name=${group[$i]}
		else
			name=${host[$i]}
		fi


		if [[ ${stat[$i]} -eq 0 ]] ; then
			ok="$ok $name"
		elif [[ ${stat[$i]} -eq 1 ]] ; then
			limbo="$limbo $name"
		else
			notok="$notok $name"
		fi

		((i++))
	done

	if [[ ${#ok} -gt 0 ]] ; then
		pp_ok "DAQ is OK on $ok." ;
	fi
	if [[ ${#limbo} -gt 0 ]] ; then
		pp_warn "DAQ is in limbo on $limbo." ;
	fi
	if [[ ${#notok} -gt 0 ]] ; then
		pp_error "DAQ is NOT OK on $notok." ;
	fi
done
