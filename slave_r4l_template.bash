#!/bin/bash

scripts=$(pwd)/$(dirname $0)/../scripts
. $scripts/drasi_common.bash

$scripts/trloii_setup.sh VULOM4 3 vulom.trlo section || exit 1

#gdb --args \
#../r3bfuser/build_cc_$(gcc -dumpmachine)_$(gcc -dumpversion)_release/m_read_meb.drasi \
../r3bfuser/build_cc_$(gcc -dumpmachine)_$(gcc -dumpversion)_debug/m_read_meb.drasi \
	--label=$this_name \
	--triva=slave,fctime=$this_fca,ctime=$this_cvt,spill1213 \
	--server=drasi,dest=$EB_HOST$EB_DRASI_PORT_SUFF \
	--buf=size=$this_buf_size \
	--max-ev-size=$this_max_ev_size \
	--max-ev-interval=35s \
	--inspill-stuck-timeout=30s \
	--subev=crate=0,type=$this_type,subtype=$this_subtype,control=$this_ctrl,procid=12 \
	--ntp=r3bntp1.gsi.de \
	--master=$MASTER
