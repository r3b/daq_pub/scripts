#!/bin/sh

set -e

source $(dirname $0)/../env/env.sh

log=trloii_setup.log

if [ 4 -gt $# ]
then
	echo TRLO II setup logging to $log. 1>&2
	echo Usage: $0 type addr [noclear] file "sections" \[trimi\]... 1>&2
	echo Example: $0 VULOM4 3 config.trlo MySection "dt_out=ECL_OUT(1)" 1>&2
	exit 1
fi
type=$1
shift
addr=$1
shift
if [ "x$1" == "xnoclear" ] ; then
	clear=""
	shift
else
	clear="--clear-setup"
fi
file=$1
shift
sections=$1
shift
trimi=
while [ 0 -lt $# ]
do
	trimi="$trimi $1"
	shift
done

echo | tee -a $log
echo $(date):$(pwd):type=\"$type\":addr=\"$addr\":file=\"$file\":sections=\"$sections\":trimi=\"$trimi\" | tee -a $log

eval \$${type}_CTRL --addr=$addr $clear --config=$file $sections 2>&1 | tee -a $log

if [ "VULOM4" = "$type" ]
then
	conn_def="ECL_IN(1)"
else
	conn_def="CTRL_IN(1)"
fi

# HTJ: changed from '[ 2 -eq $addr ] &&' to not get an error code when addr != 2

if [ 2 -eq $addr ]
then
	$TRIMI_CTRL --addr=$addr link_period=10 fast_dt=256 \
		"serial_in=$conn_def" \
		"encoded_in=TRLO_ENCODED_TRIG" \
		"dt_in=none" \
		$trimi 2>&1 | tee -a $log
fi
