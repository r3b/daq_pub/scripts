#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..

. scripts/is_host_type_.bash
. scripts/add_dir_.bash

if [ ! -e $path/master.bash ]
then
	cp scripts/master_${host_type}_template.bash $path/master.bash
	chmod u+x $path/master.bash
fi
if [ ! -e $path/main.cfg ]
then
	cp scripts/main_template.cfg $path/main.cfg
fi
