#!/bin/bash

stream=$1
shift
server=$1
shift

. ~/fake_cvmfs/sourceme.sh

while true
do
	sleep 5
	$UCESB_DIR/empty/empty --stream=$stream --server=$server
done
