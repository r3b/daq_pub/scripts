#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..

. scripts/is_host_type_.bash
. scripts/add_dir_.bash

if [ ! -e $path/slave.bash ]
then
	cp scripts/slave_${host_type}_template.bash $path/slave.bash
	chmod u+x $path/slave.bash
fi
if [ ! -e $path/main.cfg ]
then
	cp scripts/main_template.cfg $path/main.cfg
fi
