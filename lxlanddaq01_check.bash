HOST=lxlanddaq01
if [ "$(hostname -s)" != "$HOST" ]
then
	echo "This must run on $HOST!" 1>&2
	exit 1
fi
