#!/bin/bash

cd $(dirname $0)

if [ 1 -ne $# ]
then
	echo "Usage: $0 ams" 1>&2
	exit 1
fi

function link()
{
	src=drasi_${2}.bash
	dst=${1}_${2}.bash
	if [ -e $dst ]
	then
		echo "$dst: Already exists, I won't touch it, but YOU must check it!"
	else
		ln -s $src $dst
	fi
}

link $1 setup
link $1 start
link $1 killemall
link $1 match
link $1 tree
