#!/bin/bash

source $EXP_PATH/to.conf

stream=$((${TO_PORT}+2))
shift
server=$((${TO_PORT}+3))
shift

. ~/fake_cvmfs/sourceme.sh

while true
do
	sleep 5
	$UCESB_DIR/empty/empty --stream=${TO_PC}:$stream --server=stream:$server
done
