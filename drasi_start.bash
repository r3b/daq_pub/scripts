#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..
. scripts/drasi_common.bash

screen -ls | grep -q $session
echo $do_gui
if [ 0 -ne $? ]
then
	if [ "$do_gui" ]
	then
		xmessage -geometry 400x300 "Did you forget to setup the DAQ?"
	else
		echo "Did you forget to run $scripts/${branch}_setup.bash?" 1>&2
	fi
	exit 1
fi

function start()
{
	name=$1
	user=$2
	host=${1%%:*}
	script=$3
	shortify $host
	echo "Starting $host in $shortname (user is '$user')"
	if [ "x${user}" == "xprofi" ] ; then
		path=/frs/usr/profi/mbsrun
	elif [ "x${name:0:2}" != "xlx" ] ; then
		path=/land/usr/land/landexp
	else
		path=/u/land/lynx.landexp
	fi
	screen -S $session -p $shortname -X stuff "
[ \`hostname | cut -d'.' -f1 | tr '[:upper:]' '[:lower:]'\` = $host ] || ssh $host
cd $path/$exp_name/
explogin $exp_name
cd $name
./$script.bash $branch
"
}

screen -S $session -p serv -X stuff "ssh -t lxir133 $scripts/serv.bash $EB_HOST$EB_STREAM_PORT_SUFF stream$EB_SERVE_PORT_SUFF
"
screen -S $session -p log -X stuff "cd $scripts/..
./drasi/bin/lwrocmon --log=$EB_HOST$EB_DRASI_PORT_SUFF/drasi.log $EB_HOST$EB_DRASI_PORT_SUFF $HOST_ARRAY
"
sleep 1

start $EB_HOST$EB_DRASI_PORT_SUFF $(whoami) eb
start $MASTER $MASTER_USER master
for slave in $SLAVE_ARRAY
do
	host=${slave##*@}
	user=${slave%%@*}
	start $host $user slave
done

echo Done.
echo
echo "Enjoy!"
echo
