#/!bin/bash

function list
{
	echo $1
	ls -tr $1/202205_s509/lmd/*.lmd | tail -n20 | sed 's,.*/,,'
}

while true
do
	d4=$(list /d/land4)
	d5=$(list /d/land5)
	d7=$(list /d/land7)
	l=$(list /lustre/r3b)
	echo -------------------------------------------------------------------------
	paste <(printf %s "$d4") <(printf %s "$d5") <(printf %s "$d7") <(printf %s "$l")
	sleep 60
done
