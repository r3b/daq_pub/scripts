#!/bin/bash

scripts=$(pwd)/$(dirname $0)/../scripts
. $scripts/drasi_common.bash

#gdb --args \
#../nurdlib/build_cc_$(gcc -dumpmachine)_$(gcc -dumpversion)_release/m_read_meb.drasi \
../nurdlib/build_cc_$(gcc -dumpmachine)_$(gcc -dumpversion)_debug/m_read_meb.drasi \
	--label=$this_name \
	--trixor=master,fctime=$this_fca,ctime=$this_cvt,spill1213 \
	--server=drasi,dest=$EB_HOST$EB_DRASI_PORT_SUFF \
	--buf=size=$this_buf_size \
	--max-ev-size=$this_max_ev_size \
	--max-ev-interval=35s \
	--inspill-stuck-timeout=30s \
	--subev=crate=0,type=$this_type,subtype=$this_subtype,control=$this_ctrl,procid=13 \
	--ntp=r3bntp1.gsi.de \
	--eb=$EB_HOST$EB_DRASI_PORT_SUFF \
	${DRASI_SLAVE_ARRAY[*]}

