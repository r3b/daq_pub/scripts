#!/bin/bash

cd $EXP_PATH

pwd
source to.conf
scripts=${EXP_PATH}/scripts
cd $scripts/
. drasi_common.bash to
cd ../

echo
echo "This will setup drasi Time-orderer and started it."
echo

TO=TO_${EXP_NAME}

screen -ls | grep -q $TO
if [ 0 -eq $? ]
then
	ask_to_kill "Screen session $TO already running, kill?"
	echo "Killing $TO..."
	screen -S $TO -X quit
	sleep 2
fi

echo "Starting TO..."
screen -dmS $TO
screen -S $TO -p 0 -X title tree
screen -S $TO -p tree -X stuff "ssh ${TO_PC}
explogin $EXP_NAME
cd $EXP_PATH
./drasi/bin/lwrocmon ${TO_PC}:${TO_PORT} --tree
"

screen -S $TO -X screen -t to
screen -S $TO -p to -X stuff "ssh ${TO_PC}
explogin $EXP_NAME
cd $EXP_PATH
cd ${TO_PC}:${TO_PORT}
./to.bash
"

screen -S $TO -X screen -t serv
screen -S $TO -p serv -X stuff "ssh ${TO_PC} 
explogin $EXP_NAME
cd $EXP_PATH
cd ${TO_PC}:${TO_PORT}
./serv_to.bash
"
screen -S $TO -p 0 -X screen -t log
screen -S $TO -p log -X stuff "ssh ${TO_PC}
explogin $EXP_NAME
cd $EXP_PATH
./drasi/bin/lwrocmon ${TO_PC}:${TO_PORT} --log
"


screen -S $TO -X screen -t misc

echo Done.
echo
echo "To run screen, type screen -x $TO"
echo
echo "To kill and restart exectuables: enter ctrl+c and up and enter in screen"
