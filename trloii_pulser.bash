#!/bin/bash

set -e

source $(pwd)/$(dirname $0)/../env/env.sh

function usage() {
	echo "Usage: $0 type addr cmd [channel, frequency, output]"
	echo "  type = VULOM4, TRIDI"
	echo "  addr = module hex address"
	echo "  cmd ="
	echo "    status"
	echo "    periodic <channel> <frequency Hz> <output>"
	echo "    random <frequency Hz> <output>"
	echo "  channel = pulser channel, typically 1..4"
	echo "  frequency = 1MHz, 1001 etc"
	echo "  output = mux output, eg LEMO_OUT(1), TRIG_LMU_AUX(1) etc"
	echo "Example: $0 VULOM4 3 periodic 2 1MHz TRIG_LMU_AUX(2)"
}

type=$1
addr=$2
cmd=$3
if [ "$cmd" == "periodic" ] ; then
	ch=$4
	shift
fi
freq=$4
out=$5

call="${type}_CTRL"
call="${!call} --addr=$addr"

case $cmd in
	"status")
		$call --print-config | grep -E "period\($ch\)|prng|PULSER\($ch\)|PRNG|tpat_enable"
		;;
	"periodic")
		$call "${out}=PULSER(${ch}); period(${ch})=$freq Hz;"
		;;
	"random")
		$call "${out}=PRNG_POISSON(1); prng_poisson(1)=$freq Hz;"
		;;
	*)
		usage
		;;
esac
