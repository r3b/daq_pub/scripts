#!/bin/bash

scripts=$(pwd)/$(dirname $0)/../scripts
. $scripts/drasi_common.bash

../drasi/bin/lwrocmerge \
	--label=$EB_NAME \
	$EB_DRASI_PORT_OPT \
	--merge-mode=event \
	--server=drasi,dest=$TO \
	--server=trans$EB_TRANS_PORT_SUFF,flush=1 \
	--server=stream$EB_STREAM_PORT_SUFF,flush=1 \
	--buf=size=$EB_BUF_SIZE \
	--max-ev-size=$EB_MAX_EV_SIZE \
	--max-ev-interval=45s \
	--eb-master=$MASTER \
	--drasi=$MASTER \
	${DRASI_ARRAY[*]}
