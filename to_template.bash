#!/bin/bash

source ${EXP_PATH}/to.conf

HOST=${TO_PC}
if [ $(hostname -s) != "$HOST" ]
then
	echo "Must run on $HOST!" 1>&2
	exit 1
fi

TO_NAME=TO_${EXP_NAME}
trans=$((${TO_PORT}+1))
stream=$((${TO_PORT}+2))

outbuf="bufsize=200Mi"

# basic setup
TO="../drasi/bin/lwrocmerge"
TO+=" --label=${TO_NAME}"
TO+=" --port=${TO_PORT}"
TO+=" --merge-mode=wr"
TO+=" --server=$outbuf,trans:$trans,nohold"
TO+=" --server=$outbuf,stream:$stream"
TO+=" --file-writer=$outbuf"
TO+=" --buf=size=25Gi"
TO+=" --max-ev-size=130Mi"
TO+=" --merge-ts-analyse-ref=16"
TO+=" --merge-ts-analyse-sync-trig=3"


# event sources - add as become available
# TO+=" --drasi=ts-disable=60s,inbufsize=2000Mi,eb_path" #det system
TO+=" --drasi=ts-disable=60s,inbufsize=2000Mi,lxir133:7770" # Main

echo -n  "Running "
echo "$TO" | sed 's/ /\n      /g'

exec $TO

exit
