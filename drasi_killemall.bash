#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..
. scripts/drasi_common.bash

ask_to_kill "This will kill the DAQ but keep the screen session, continue?"

kill -9 $(ps auxw | grep "log=$EB_HOST$EB_DRASI_PORT_SUFF" | grep drasi.log | awk '{print $2}') 2> /dev/null
for i in 1 2
do
	echo "Take $i..."
	for host in serv $EB_HOST $MASTER ${SLAVE[@]}
	do
		host=${host%%:*}
		shortify $host
		echo Killing $host
		if [ "x${host:0:2}" != "xlx" ]
		then
			path=/land/usr/land/
		else
			path=/u/land/lynx.usr/land/
		fi
		for j in 1 2 3
		do
			screen -S $session -p $shortname -X stuff 
		done
	done
	wait
done

echo Done.
echo
echo "Run $scripts/${branch}_start.bash to have fun again!"
echo
