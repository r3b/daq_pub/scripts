#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..
. scripts/drasi_common.bash

bash scripts/add_eb.bash $EB_HOST$EB_DRASI_PORT_SUFF
bash scripts/add_master.bash $MASTER
for slave in $SLAVE_ARRAY
do
	host=${slave##*@}
	bash scripts/add_slave.bash $host
done
