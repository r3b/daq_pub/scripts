#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..
. scripts/drasi_common.bash
./drasi/bin/lwrocmon --tree $EB_HOST$EB_DRASI_PORT_SUFF $MASTER ${SLAVE[*]}
