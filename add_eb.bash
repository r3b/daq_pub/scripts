#!/bin/bash

scripts=$(pwd)/$(dirname $0)
cd $scripts/..

. scripts/add_dir_.bash

if [ ! -e $path/eb.bash ]
then
	cp scripts/eb_template.bash $path/eb.bash
	chmod u+x $path/eb.bash
fi
